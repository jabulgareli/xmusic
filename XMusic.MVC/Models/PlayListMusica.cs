﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace XMusic.MVC.Models
{
    [Table("PlayListMusica")]
    public class PlayListMusica
    {
        [Key, Column(Order = 0)]
        public int PlayListId { get; set; }
        [Key, Column(Order = 1)]
        public int MusicaId { get; set; }

        [ForeignKey("PlayListId")]
        public virtual PlayList PlayList { get; set; }
        [ForeignKey("MusicaId")]
        public virtual Musica Musica { get; set; }
    }
}