﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace XMusic.MVC.Models
{
    [Table("Usuario")]
    public class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UsuarioId { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "O campo deve conter no mínimo 6 caracteres")]
        [MaxLength(50, ErrorMessage = "O campo pode conter no máximo 50 caracteres")]
        public string Nome { get; set; }
        [Required]
        [EmailAddress]
        [MaxLength(50, ErrorMessage = "O campo pode conter no máximo 50 caracteres")]
        public string Email { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "O campo deve conter no mínimo 6 caracteres")]
        [MaxLength(32, ErrorMessage = "O campo pode conter no máximo 32 caracteres")]
        public string Senha { get; set; }
        public virtual List<PlayList> PlayLists { get; set; }
    }
}