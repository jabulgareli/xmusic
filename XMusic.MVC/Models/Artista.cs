﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace XMusic.MVC.Models
{
    [Table("Artista")]
    public class Artista
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ArtistaId { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "O campo deve conter no mínimo 6 caracteres")]
        [MaxLength(50, ErrorMessage = "O campo pode conter no máximo 50 caracteres")]
        public string Nome { get; set; }
        public DateTime InicioCarreira { get; set; }

        public virtual List<Album> Albuns { get; set; }
    }
}