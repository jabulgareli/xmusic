﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace XMusic.MVC.Models
{
    [Table("Album")]
    public class Album
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AlbumId { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "O campo deve conter no mínimo 6 caracteres")]
        [MaxLength(32, ErrorMessage = "O campo pode conter no máximo 32 caracteres")]
        public string Nome { get; set; }
        public virtual List<Musica> Musicas { get; set; }

        public int ArtistaId { get; set; }
        [ForeignKey("ArtistaId")]
        public virtual Artista Artista { get; set; }
    }
}