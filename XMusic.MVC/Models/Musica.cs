﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace XMusic.MVC.Models
{
    [Table("Musica")]
    public class Musica
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MusicaId { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "O campo deve conter no mínimo 6 caracteres")]
        [MaxLength(50, ErrorMessage = "O campo pode conter no máximo 50 caracteres")]
        public string Titulo { get; set; }
        public int AlbumId { get; set; }
        [ForeignKey("AlbumId")]
        public virtual Album Album { get; set; }
        public int? ConvidadoId { get; set; }
        [ForeignKey("ConvidadoId")]
        public virtual Artista Convidado { get; set; }
        public int GeneroId { get; set; }
        [ForeignKey("GeneroId")]
        public virtual Genero Genero { get; set; }
        public Idioma Idioma { get; set; }
        public virtual List<PlayListMusica> Playlists { get; set; }

        public string GetTituloFormatado()
        {
            return Album.Artista.Nome + (Convidado != null ? " part. esp." + Convidado.Nome : "") + " - " + Titulo;
        }
    }
}