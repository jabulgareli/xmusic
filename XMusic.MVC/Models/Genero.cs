﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace XMusic.MVC.Models
{
    [Table("Genero")]
    public class Genero
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GeneroId { get; set; }
        [Required]
        [MinLength(3, ErrorMessage = "O campo deve conter no mínimo 3 caracteres")]
        [MaxLength(50, ErrorMessage = "O campo pode conter no máximo 50 caracteres")]
        public string Nome { get; set; }
    }
}