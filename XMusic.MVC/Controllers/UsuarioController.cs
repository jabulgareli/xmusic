﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XMusic.MVC.Models;
using XMusic.MVC.Persistencia;

namespace XMusic.MVC.Controllers
{
    public class UsuarioController : Controller
    {
        private PersistenciaUsuario persistenciaUsuario;
        private ContextoEF contextoEF;

        public UsuarioController()
        {
            persistenciaUsuario = new PersistenciaUsuario();
            contextoEF = new ContextoEF();
        }
        // GET: Usuario
        public ActionResult Index()
        {
            //ArtistaId para ConvidadoId
            //Idioma para integer
            //Remover PlayListMusicaId

            var usuario = contextoEF.Usuarios.FirstOrDefault(u => u.Email.Equals("jabulgareli@gmail.com"));

            var artista = new Artista { InicioCarreira = DateTime.Now.AddYears(-4), Nome = "Anitta" };
            contextoEF.Artistas.Add(artista);
            contextoEF.SaveChanges();

            var artistaConvidado = new Artista { InicioCarreira = DateTime.Now.AddYears(-4), Nome = "MC Catra" };
            contextoEF.Artistas.Add(artistaConvidado);
            contextoEF.SaveChanges();

            var genero = new Genero { Nome = "Funk" };
            contextoEF.Generos.Add(genero);
            contextoEF.SaveChanges();

            var album = new Album { Artista = artista, Nome = "Show das poderosas" };
            contextoEF.Albuns.Add(album);
            contextoEF.SaveChanges();

            contextoEF.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

            //var artista = contextoEF.Artistas.FirstOrDefault(a => a.Nome.Equals("Anitta"));
            //var artistaConvidado = contextoEF.Artistas.FirstOrDefault(a => a.Nome.Equals("MC Catra"));
            //var genero = contextoEF.Generos.FirstOrDefault(g => g.Nome.Equals("Funk"));
            //var album = contextoEF.Albuns.FirstOrDefault(a => a.Nome.Equals("Show das poderosas"));


            album.Musicas = new List<Musica>();
            var musicaPrepara = new Musica { Album = album, Genero = genero, Idioma = Idioma.Portugues, Titulo = "Prepara" };
            album.Musicas.Add(musicaPrepara);
            var musicaMimimi = new Musica { Album = album, Genero = genero, Idioma = Idioma.Portugues, Titulo = "Mimimi", Convidado = artistaConvidado };
            album.Musicas.Add(musicaMimimi);
            contextoEF.SaveChanges();

            var playlist = new PlayList();
            playlist.Titulo = "Músicas da Anitta";
            playlist.Usuario = usuario;
            playlist.Musicas = new List<PlayListMusica>();
            playlist.Musicas.Add(new PlayListMusica { PlayList = playlist, Musica = musicaPrepara });
            playlist.Musicas.Add(new PlayListMusica { PlayList = playlist, Musica = musicaMimimi });
            contextoEF.PlayLists.Add(playlist);
            contextoEF.SaveChanges();

            return View();
        }

        public ActionResult ObterTodos()
        {
            var todosUsuarios = persistenciaUsuario.ObterTodos();
            return View(todosUsuarios);
        }

        public ActionResult ObterTodosEF()
        {
            var todosUsuarios = contextoEF.Usuarios.ToList();
            return View(todosUsuarios);
        }

        public ActionResult Adicionar()
        {
            var usuario = new Usuario();
            usuario.Nome = "Elvis Lima";
            usuario.Email = "elvis.lima@linx.com.br";
            usuario.Senha = "123456";

            var retorno = persistenciaUsuario.Adicionar(usuario);

            return View(retorno);
        }

        public ActionResult AdicionarEF()
        {
            var usuario = new Usuario();
            usuario.Nome = "João Bulgareli";
            usuario.Email = "jabulgareli@gmail.com";
            usuario.Senha = "123456";

            contextoEF.Usuarios.Add(usuario);
            contextoEF.SaveChanges();
            return View(usuario);
        }

        public ActionResult Atualizar(int usuarioId)
        {
            var retorno = 0;
            var usuario = persistenciaUsuario.Obter(usuarioId);

            if (usuario != null)
            {
                usuario.Nome = "Elvis";
                usuario.Senha = "654321";

                retorno = persistenciaUsuario.Atualizar(usuario);
            }

            return View(retorno);
        }

        public ActionResult AtualizarEF()
        {
            var usuario = contextoEF.Usuarios.FirstOrDefault(u => u.Email.Equals("jabulgareli@gmail.com"));

            usuario.Nome = "Joao Bulgareli";
            usuario.Senha = "010101";

            contextoEF.SaveChanges();

            return View(usuario);
        }

        public ActionResult Remover(int usuarioId)
        {
            var retorno = persistenciaUsuario.Remover(usuarioId);

            return View(retorno);
        }

        public ActionResult RemoverEF()
        {
            var usuario = contextoEF.Usuarios.FirstOrDefault(u => u.Email.Equals("jabulgareli@gmail.com"));
            if (usuario != null)
            {
                contextoEF.Usuarios.Remove(usuario);
                contextoEF.SaveChanges();
            }

            return View();
        }
    }
}