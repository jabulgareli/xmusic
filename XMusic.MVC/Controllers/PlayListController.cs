﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XMusic.MVC.Models;
using XMusic.MVC.Persistencia;

namespace XMusic.MVC.Controllers
{
    public class PlayListController : Controller
    {
        private PersistenciaPlayList persistenciaPlayList = new PersistenciaPlayList();

        // GET: PlayList
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObterTodos()
        {
            var todasPlayList = persistenciaPlayList.ObterTodasPorUsuario(4);
            return View(todasPlayList);
        }

        public ActionResult Adicionar()
        {
            var playList = new PlayList();
            playList.Titulo = "PlayList X";
            playList.UsuarioId = 4;

            var retorno = persistenciaPlayList.Adicionar(playList);

            return View(retorno);
        }

        public ActionResult Atualizar(int playListId)
        {
            var retorno = 0;
            var playList = persistenciaPlayList.Obter(playListId);

            if (playList != null)
            {
                playList.Titulo = "PlayList XX";
                playList.UsuarioId = 4;

                retorno = persistenciaPlayList.Atualizar(playList);
            }

            return View(retorno);
        }

        public ActionResult Remover(int playListId)
        {
            var retorno = persistenciaPlayList.Remover(playListId);

            return View(retorno);
        }
    }
}