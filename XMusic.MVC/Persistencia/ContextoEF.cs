﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using XMusic.MVC.Models;

namespace XMusic.MVC.Persistencia
{
    public class ContextoEF : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<PlayList> PlayLists { get; set; }
        public DbSet<Musica> Musicas { get; set; }
        public DbSet<Genero> Generos { get; set; }
        public DbSet<Artista> Artistas { get; set; }
        public DbSet<Album> Albuns { get; set; }

        public ContextoEF() : base("ContextoXMusic")
        {

        }
    }
}