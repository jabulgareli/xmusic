﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using XMusic.MVC.Models;
using System.Diagnostics;
using System.Data;

namespace XMusic.MVC.Persistencia
{
    public class PersistenciaPlayList
    {
        private SqlConnection conexao;
        private PersistenciaPlayListMusica persistenciaPlayListMusica = new PersistenciaPlayListMusica();

        public PersistenciaPlayList()
        {
            var stringConexao = System.Configuration.ConfigurationManager.ConnectionStrings["ContextoXMusic"].ConnectionString;
            conexao = new SqlConnection(stringConexao);
            conexao.StateChange += Conexao_StateChange;
        }

        private void Conexao_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            switch (e.CurrentState)
            {
                case ConnectionState.Connecting:
                    Debug.WriteLine("XMUSIC - Abrindo conexão...");
                    break;
                case ConnectionState.Open:
                    Debug.WriteLine("XMUSIC - Aberta!");
                    break;
                case ConnectionState.Closed:
                    Debug.WriteLine("XMUSIC - Fechada!");
                    break;
                case ConnectionState.Executing:
                    Debug.WriteLine("XMUSIC - Executando...");
                    break;
            }
        }


        public List<PlayList> ObterTodasPorUsuario(int usuarioId)
        {
            try
            {
                var retorno = new List<PlayList>();
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM PlayList WHERE UsuarioId = @UsuarioId";
                    cmd.Parameters.AddWithValue("UsuarioId", usuarioId);

                    var resultado = cmd.ExecuteReader();

                    while (resultado.Read())
                    {
                        var playList = new PlayList();

                        playList.PlayListId = int.Parse(resultado["PlayListId"].ToString());
                        playList.Titulo = resultado["Titulo"].ToString();
                        playList.UsuarioId = int.Parse(resultado["UsuarioId"].ToString());
                        playList.Musicas = persistenciaPlayListMusica.ObterTodasPorPlayList(playList.PlayListId);

                        retorno.Add(playList);
                    }

                    return retorno;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public PlayList Obter(int playListId)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM PlayList WHERE PlayListId = @PlayListId";
                    cmd.Parameters.AddWithValue("PlayListId", playListId);

                    var resultado = cmd.ExecuteReader();

                    if (resultado.Read())
                    {
                        var playList = new PlayList();

                        playList.PlayListId = int.Parse(resultado["PlayListId"].ToString());
                        playList.Titulo = resultado["Titulo"].ToString();
                        playList.UsuarioId = int.Parse(resultado["UsuarioId"].ToString());
                        playList.Musicas = persistenciaPlayListMusica.ObterTodasPorPlayList(playList.PlayListId);

                        return playList;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Adicionar(PlayList playList)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO PlayList (Titulo, UsuarioId) VALUES (@Titulo, @UsuarioId)";

                    cmd.Parameters.AddWithValue("Titulo", playList.Titulo);
                    cmd.Parameters.AddWithValue("UsuarioId", playList.UsuarioId);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Atualizar(PlayList playList)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "UPDATE PlayList SET Titulo = @Titulo WHERE PlayListId = @PlayListId";

                    cmd.Parameters.AddWithValue("Titulo", playList.Titulo);
                    cmd.Parameters.AddWithValue("PlayListId", playList.PlayListId);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Remover(int playListId)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM PlayList WHERE PlayListId = @PlayListId";
                    cmd.Parameters.AddWithValue("PlayListId", playListId);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }
    }
}