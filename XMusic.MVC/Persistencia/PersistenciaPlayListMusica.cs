﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using XMusic.MVC.Models;
using System.Diagnostics;
using System.Data;

namespace XMusic.MVC.Persistencia
{
    public class PersistenciaPlayListMusica
    {
        private SqlConnection conexao;

        public PersistenciaPlayListMusica()
        {
            var stringConexao = System.Configuration.ConfigurationManager.ConnectionStrings["ContextoXMusic"].ConnectionString;
            conexao = new SqlConnection(stringConexao);
            conexao.StateChange += Conexao_StateChange;
        }

        private void Conexao_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            switch (e.CurrentState)
            {
                case ConnectionState.Connecting:
                    Debug.WriteLine("XMUSIC - Abrindo conexão...");
                    break;
                case ConnectionState.Open:
                    Debug.WriteLine("XMUSIC - Aberta!");
                    break;
                case ConnectionState.Closed:
                    Debug.WriteLine("XMUSIC - Fechada!");
                    break;
                case ConnectionState.Executing:
                    Debug.WriteLine("XMUSIC - Executando...");
                    break;
            }
        }


        public List<PlayListMusica> ObterTodasPorPlayList(int playListId)
        {
            try
            {
                var retorno = new List<PlayListMusica>();
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM PlayListMusica WHERE PlayListId = @PlayListId";
                    cmd.Parameters.AddWithValue("PlayListId", playListId);

                    var resultado = cmd.ExecuteReader();

                    while (resultado.Read())
                    {
                        var playListMusica = new PlayListMusica();

                        playListMusica.MusicaId = int.Parse(resultado["MusicaId"].ToString());
                        playListMusica.PlayListId = int.Parse(resultado["PlayListId"].ToString());

                        retorno.Add(playListMusica);
                    }

                    return retorno;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public PlayListMusica Obter(int musicaId, int playListId)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM PlayListMusica WHERE MusicaId = @MusicaId AND PlayListId = @PlayListId";
                    cmd.Parameters.AddWithValue("MusicaId", musicaId);
                    cmd.Parameters.AddWithValue("PlayListId", playListId);

                    var resultado = cmd.ExecuteReader();

                    if (resultado.Read())
                    {
                        var playList = new PlayListMusica();

                        playList.MusicaId = int.Parse(resultado["MusicaId"].ToString());
                        playList.PlayListId = int.Parse(resultado["PlayListId"].ToString());

                        return playList;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Adicionar(PlayListMusica playListMusica)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO PlayListMusica (MusicaId, PlayListId) VALUES (@MusicaId, @PlayListId)";

                    cmd.Parameters.AddWithValue("MusicaId", playListMusica.MusicaId);
                    cmd.Parameters.AddWithValue("PlayListId", playListMusica.PlayListId);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Remover(int musicaId, int playListId)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM PlayListMusica WHERE MusicaId = @MusicaId AND PlayListId = @PlayListId";

                    cmd.Parameters.AddWithValue("MusicaId", musicaId);
                    cmd.Parameters.AddWithValue("PlayListId", playListId);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }
    }
}