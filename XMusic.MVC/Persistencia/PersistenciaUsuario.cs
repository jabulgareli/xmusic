﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using XMusic.MVC.Models;
using System.Diagnostics;
using System.Data;

namespace XMusic.MVC.Persistencia
{
    public class PersistenciaUsuario
    {
        private SqlConnection conexao;
        private PersistenciaPlayList persistenciaPlaylist = new PersistenciaPlayList();

        public PersistenciaUsuario()
        {
            var stringConexao = System.Configuration.ConfigurationManager.ConnectionStrings["ContextoXMusic"].ConnectionString;
            conexao = new SqlConnection(stringConexao);
            conexao.StateChange += Conexao_StateChange;
        }

        private void Conexao_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            switch (e.CurrentState)
            {
                case ConnectionState.Connecting:
                    Debug.WriteLine("XMUSIC - Abrindo conexão...");
                    break;
                case ConnectionState.Open:
                    Debug.WriteLine("XMUSIC - Aberta!");
                    break;
                case ConnectionState.Closed:
                    Debug.WriteLine("XMUSIC - Fechada!");
                    break;
                case ConnectionState.Executing:
                    Debug.WriteLine("XMUSIC - Executando...");
                    break;
            }
        }


        public List<Usuario> ObterTodos()
        {
            try
            {
                var retorno = new List<Usuario>();
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM Usuario";
                    var resultado = cmd.ExecuteReader();

                    while (resultado.Read())
                    {
                        var usuario = new Usuario();
                        usuario.Email = resultado["Email"].ToString();
                        usuario.Nome = resultado["Nome"].ToString();
                        usuario.Senha = resultado["Senha"].ToString();
                        usuario.UsuarioId = int.Parse(resultado["UsuarioId"].ToString());
                        usuario.PlayLists = persistenciaPlaylist.ObterTodasPorUsuario(usuario.UsuarioId);
                        retorno.Add(usuario);
                    }

                    return retorno;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Usuario Obter(int usuarioId)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM Usuario WHERE UsuarioId = @UsuarioId";
                    cmd.Parameters.AddWithValue("UsuarioId", usuarioId);

                    var resultado = cmd.ExecuteReader();

                    if (resultado.Read())
                    {
                        var usuario = new Usuario();

                        usuario.Email = resultado["Email"].ToString();
                        usuario.Nome = resultado["Nome"].ToString();
                        usuario.Senha = resultado["Senha"].ToString();
                        usuario.UsuarioId = int.Parse(resultado["UsuarioId"].ToString());
                        usuario.PlayLists = persistenciaPlaylist.ObterTodasPorUsuario(usuario.UsuarioId);

                        return usuario;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Adicionar(Usuario usuario)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "INSERT INTO Usuario (Nome, Email, Senha) VALUES (@Nome, @Email, @Senha)";

                    cmd.Parameters.AddWithValue("Nome", usuario.Nome);
                    cmd.Parameters.AddWithValue("Email", usuario.Email);
                    cmd.Parameters.AddWithValue("Senha", usuario.Senha);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Atualizar(Usuario usuario)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "UPDATE Usuario SET Nome = @Nome, Email = @Email, Senha = @Senha WHERE UsuarioId = @UsuarioId";

                    cmd.Parameters.AddWithValue("Nome", usuario.Nome);
                    cmd.Parameters.AddWithValue("Email", usuario.Email);
                    cmd.Parameters.AddWithValue("Senha", usuario.Senha);
                    cmd.Parameters.AddWithValue("UsuarioId", usuario.UsuarioId);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        public int Remover(int usuarioId)
        {
            try
            {
                conexao.Open();

                using (var cmd = conexao.CreateCommand())
                {
                    cmd.CommandText = "DELETE FROM Usuario WHERE UsuarioId = @UsuarioId";
                    cmd.Parameters.AddWithValue("UsuarioId", usuarioId);

                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }
    }
}